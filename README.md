# Wedoogift DevOps challenge
You are interested in joining our DevOps team ? try to accomplish this challenge, we will be glad to see
your work and give you a feedback.

## Expected deliverable
* Architecture Diagram with the relevant AWS components, you can choose any tools to create your diagram. (Must Have)
* Describe why you chose each component. (Must Have)
* CloudFormation template corresponding to your architecture. (Optional)

## Problem to solve
![offerwall](assets/offerwall.png)
Using the search bar on the Offerwall page, users can find the shop they are looking for.
To improve our offer, we want to know what users are looking for. 
Everytime a user write a brand in the search bar, we want to log it somewhere. Then we want to be able to have some insights on the searched brands.
Your mission is to suggest a cloud architecture on AWS to help developers implementing the solution.

## Solution proposition
![architecture](solution/architectural%20solution.drawio.png)\
CloudWatch is a powerful tool for handling logs and metrics. We can send it custom log message that we can filter and extract data thanks to CloudWatch Logs Insight.\
A CloudWatch time event can trigger automaticaly an AWS function, here a Lambda that is great and cheap for running simple function.\
The Lambda can them execute a query in the logs generated previously, process them into the format needed and send them where we want. I choose S3 here because of the reliability and the ability to keep large files.\
We could also choose to send them in a database service like RDS or DynamoDB depending on how we want to use the data./

Here is a cloudformation proposition for the scheduled lambda part.\
```yaml
Description: >
  This CloudFormation template creates a Lambda function triggered by the
  CloudWatch Scheduled Events, which access CloudWatch Logs and store result in
  S3
Resources:
  LogResult:
    Type: 'AWS::S3::Bucket'
    Properties:
      BucketName: search-log-result
  LambdaFunctionRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Path: /
      Policies:
        - PolicyName: LambdaFunctionPolicy
          PolicyDocument:
            Statement:
              - Effect: Allow
                Action:
                  - 'logs:CreateLogGroup'
                  - 'logs:CreateLogStream'
                  - 'logs:PutLogEvents'
                Resource: '*'
              - Effect: Allow
                Action:
                  - 's3:*'
                Resource:
                    - !Sub arn:aws:s3:::${LogResult}
                    - !Sub arn:aws:s3:::${LogResult}/*
              - Effect: Allow
                Action:
                  - 'logs:DescribeLogStreams'
                  - 'logs:StartQuery'
                  - 'logs:GetQueryResults'
                  - 'logs:StopQuery'
  LambdaFunction:
    Type: 'AWS::Lambda::Function'
    Properties:
      Timeout: 900
      Handler: index.handler
      Role: !GetAtt LambdaFunctionRole.Arn
    DependsOn:
      - LogResult
  ScheduledRule:
    Type: 'AWS::Events::Rule'
    Properties:
      Description: ScheduledRule
      ScheduleExpression: rate(1 day)
      State: ENABLED
      Targets:
        - Arn:
            'Fn::GetAtt':
              - LambdaFunction
              - Arn
          Id: TargetFunctionV1
  PermissionForEventsToInvokeLambda:
    Type: 'AWS::Lambda::Permission'
    Properties:
      FunctionName:
        Ref: LambdaFunction
      Action: 'lambda:InvokeFunction'
      Principal: events.amazonaws.com
      SourceArn:
        'Fn::GetAtt':
          - ScheduledRule
          - Arn
Outputs:
  S3Bucket:
    Description: Bucket Created using this template.
    Value: !Ref LogResult

```
